﻿import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  

  email:string;
  password:string; 
  errorMessage:string; 
  isError:boolean = false; 
  
  constructor(private authService:AuthService, private router:Router) { }

  onSubmit(){
    this.authService.SignUp(this.email,this.password).then(
      res =>{
        console.log('Succesful login;');
        this.router.navigate(['/books']); 
      }
    ).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }
  
  

  ngOnInit(): void {
  }

}
