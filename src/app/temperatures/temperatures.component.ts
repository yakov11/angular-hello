import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image:string;
  country;
  lat;number;
  lon:number;
  weatherData$:Observable<Weather>
  hasError:boolean = false;
  errorMessage:string;
  constructor(private route: ActivatedRoute, private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.weatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
                this.temperature=data.temperature;
                this.temperature=  Math.round(this.temperature);
                this.image=data.image;
                this.country= data.country;
                this.lat= data.lat;
                this.lon= data.lon;

              },
      error => {
        console.log(error.message);
        this.hasError=true;
        this.errorMessage= error.message;
      }
        )
   }
}
