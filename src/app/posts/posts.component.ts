import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PostsRaw } from '../interfaces/posts-raw';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  [x: string]: any;

  userId:number;
  id:number;
  title:string;
  body:string;
  postsData$:Observable<PostsRaw>
  hasError:boolean = false;

  constructor(private route: ActivatedRoute, private postsService:PostsService) { }
  

  ngOnInit(): void {
    this.postsData$ = this.postsService.searchPostsData();
    this.postsData$.subscribe(
      data => {
                this.userId=data.userId;
                this.id=  this.id;
                this.title=data.title;
                this.body= data.body;
              },
      error => {
        console.log(error.message);
        this.hasError=true;
      })
}
}
