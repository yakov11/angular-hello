import {  OnInit } from '@angular/core';
import {Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';




@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  favoriteSeason: string;
  radios: string[] = ['BBC', 'CNN', 'NBC'];
  
  constructor(private route:ActivatedRoute) { }
  answer; 
  bbc:string;
  invalid: string;
  show: boolean = false; 
  ngOnInit(): void {
    this.bbc = this.route.snapshot.params.network;

    if (this.bbc.toUpperCase()!='BBC' && this.bbc.toUpperCase()!='CNN' && this.bbc.toUpperCase()!='NBC' ) {
      this.show = true
      this.invalid=this.bbc;
    }
    else{
      this.bbc=this.bbc.toUpperCase();
    }
  }


  }


