import { throwError, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PostsRaw } from './interfaces/posts-raw';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  private URL = " https://jsonplaceholder.typicode.com/posts/"; 
  
  constructor(private http:HttpClient) {}

  searchPostsData():Observable<PostsRaw>{
    return this.http.get<PostsRaw>(`${this.URL}`).pipe(
      catchError(this.handleError)
    )
  }
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server erorr'); 
  }

}
