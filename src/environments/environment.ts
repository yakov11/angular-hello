// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDONQiplVr1okjghGYl06L1hlgl-l4sJE4",
    authDomain: "hello-d5699.firebaseapp.com",
    databaseURL: "https://hello-d5699.firebaseio.com",
    projectId: "hello-d5699",
    storageBucket: "hello-d5699.appspot.com",
    messagingSenderId: "356971361494",
    appId: "1:356971361494:web:cb60f3b0942157ab980577",
    measurementId: "G-1ESL7CPYBN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
